This repository is an experiment building Transmission for Flatpak,
Snap and Docker.

### Dependencies

First install BuildStream 1.4 and bst-external:

```
pip3 install --user git+https://gitlab.com/BuildStream/buildstream.git@bst-1
pip3 install --user git+https://gitlab.com/BuildStream/bst-external.git@valentindavid/oci-exposed-ports-typo
```

Then build the `19.08` branch of Freedesktop SDK and install it:

```
git clone -b 19.08 https://gitlab.com/freedesktop-sdk/freedesktop-sdk.git freedesktop-sdk-snap
cd freedesktop-sdk-snap
make export-snap
snap install --dangerous snap/platform.snap
```

### Building the application

```
git clone https://gitlab.com/valentindavid/transmission-buildstream.git
cd transmission-buildstream
bst build transmission.bst
```



### Testing the application

```
bst shell transmission.bst -- transmission-gtk
```

### Building, installing and running the snap

```
bst build snap/image.bst
bst checkout snap/image.bst snap/
snap install --dangerous snap/transmission.snap

/snap/bin/transmission
```

### Building, installing and running the flatpak

```
bst build flatpak/repo.bst
bst checkout flatpak/repo.bst repo/
flatpak remote-add --no-gpg-verify local-transmission repo/
flatpak install local-firefox com.transmissionbt.Transmission

flatpak run com.transmissionbt.Transmission
```

### Building, installing and running the Docker image

```
bst build oci/transmission-docker.bst
bst checkout oci/transmission-docker.bst --tar transmission-docker.tar
docker load -i transmission-docker.tar

docker run --name transmission -d transmission/amd64 --allowed
xdg-open http://localhost:$(docker port transmission | cut -d: -f2)/
```
